clean:
	find . -name '*.pyc' -delete
	docker system prune

init:
	docker-compose build

test:
	-pytest

run:
	docker-compose up -d

stop:
	docker-compose down

check-types:
	mypy src/

check-styles:
	flake8 src/ tests/