from dataclasses import dataclass
from datetime import date
from typing import Optional, List


@dataclass
class Person:
    id: str
    name: str
    birth_date: Optional[date] = None
    city: Optional[str] = None
    country: Optional[str] = None

    BIRTH_DATE_FORMAT = '%Y%m%d'

    @property
    def age(self) -> Optional[int]:
        if not self.birth_date:
            return None
        today = date.today()
        return today.year - self.birth_date.year - (
                (today.month, today.day) < (self.birth_date.month, self.birth_date.day))

    @property
    def _days_before_birthday(self) -> int:
        """Calculates number of days before nearest person's birthday"""

        today = date.today()
        try:
            current_year_date = date(today.year, self.birth_date.month, self.birth_date.day)  # type: ignore
            next_year_date = date(today.year + 1, self.birth_date.month, self.birth_date.day)  # type: ignore
        # in case of leap year
        except ValueError:
            current_year_date = date(today.year, self.birth_date.month, self.birth_date.day - 1)  # type: ignore
            next_year_date = date(today.year + 1, self.birth_date.month, self.birth_date.day - 1)  # type: ignore

        return ((current_year_date if current_year_date > today else next_year_date) - today).days


@dataclass
class PersonsResponse:
    start: Optional[int] = None
    limit: Optional[int] = None
    count: Optional[int] = None
    previous: Optional[str] = None
    next: Optional[str] = None
    data: Optional[List[Person]] = None
