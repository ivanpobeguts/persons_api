import logging
from typing import List, Dict, Optional

from flask import current_app as app
from flask_apispec import marshal_with, doc
from flask_apispec.views import MethodResource
from flask_restful import abort, Resource, request

from .exceptions import DataError
from .models import Person, PersonsResponse
from .repository import PersonRepository
from .schema import PersonSchema, ResponsePersonsSchema

LOGGER = logging.getLogger(__name__)


class BaseResource(MethodResource, Resource):
    @staticmethod
    def get_person_repo() -> PersonRepository:  # type: ignore[return]
        try:
            return PersonRepository()
        except DataError as de:
            LOGGER.error(de)
            abort(400, error_message=str(de))
        except Exception as e:
            LOGGER.error(str(e))
            abort(400, error_message='Unknown data error')


class PersonResource(BaseResource):

    @marshal_with(PersonSchema)
    @doc(description='Get person by id')
    def get(self, person_id: str) -> Optional[Person]:
        repo = self.get_person_repo()
        person = repo.get_by_id(person_id)
        if not person:
            abort(404)
        return person


class PersonListResource(BaseResource):

    @marshal_with(ResponsePersonsSchema)
    @doc(description='Get all persons',
         params={
             'order_by': {'description': 'Order by attribute', 'in': 'query', 'type': 'string', 'required': False},
             'reverse': {'description': 'Sorting order (false/true)', 'in': 'query', 'type': 'string',
                         'required': False},
             'country': {'description': 'Get persons by country', 'in': 'query', 'type': 'string', 'required': False},
             'limit': {'description': 'Page limit', 'in': 'query', 'type': 'string', 'required': False},
             'start': {'description': 'Start page', 'in': 'query', 'type': 'string', 'required': False},
         })
    def get(self) -> PersonsResponse:
        repo = self.get_person_repo()
        args = request.args
        persons = self._get_persons_by_args(args, repo)
        response_obj = self._get_paginated_list(persons, request.base_url, int(args.get('start', 1)),
                                                int(args.get('limit', app.config['PAGE_SIZE'])))
        return response_obj

    def _get_persons_by_args(self, args: Dict, repo: PersonRepository) -> List[Person]:
        """Gets persons list depending on query parameters"""

        if 'country' in args:
            return list(repo.get_by_country(args['country']))
        persons = repo.get()
        if 'order_by' in args:
            order_field = args['order_by']
            reverse_arg = args.get('reverse')
            reverse = False if not reverse_arg or reverse_arg.upper() == 'FALSE' else True
            try:
                return sorted(persons, key=lambda p: getattr(p, order_field), reverse=reverse)
            except AttributeError:
                return persons
        return persons

    @staticmethod
    def _get_paginated_list(persons: List[Person], url: str, start: int, limit: int) -> PersonsResponse:
        """Constructs paginated response object"""

        count = len(persons)

        response_obj = PersonsResponse()
        response_obj.start = start
        response_obj.limit = limit
        response_obj.count = count

        if start == 1:
            response_obj.previous = ''
        else:
            start_copy = max(1, start - limit)
            limit_copy = start - 1
            response_obj.previous = url + '?start=%d&limit=%d' % (start_copy, limit_copy)

        if start + limit > count:
            response_obj.next = ''
        else:
            start_copy = start + limit
            response_obj.next = url + '?start=%d&limit=%d' % (start_copy, limit)

        response_obj.data = persons[(start - 1):(start - 1 + limit)]
        return response_obj


class NextBirthdayPersonsResource(BaseResource):

    @marshal_with(PersonSchema(many=True))
    @doc(description='Get next birthday person(s)')
    def get(self) -> List[Person]:
        repo = self.get_person_repo()
        persons = repo.get()
        return self._get_next_birthday_persons(persons)

    @staticmethod
    def _get_next_birthday_persons(persons: List[Person]) -> List[Person]:
        sorted_persons = sorted(persons, key=lambda p: p._days_before_birthday)
        min_value = sorted_persons[0]._days_before_birthday
        last_index = 0
        for index, person in enumerate(sorted_persons):
            if person._days_before_birthday > min_value:
                last_index = index
                break
        return sorted_persons[:last_index]
