from marshmallow import fields, Schema


class PersonSchema(Schema):
    class Meta:
        fields = ('id', 'name', 'age', 'birth_date', 'city', 'country')


class ResponsePersonsSchema(Schema):
    start = fields.Int()
    limit = fields.Int()
    count = fields.Int()
    previous = fields.Str()
    next = fields.Str()
    data = fields.Nested(PersonSchema, many=True)
