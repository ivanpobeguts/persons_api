import json

from apispec import APISpec
from apispec.ext.marshmallow import MarshmallowPlugin
from flask_apispec.extension import FlaskApiSpec
from flask import Flask
from flask_restful import Api

from .views import NextBirthdayPersonsResource, PersonResource, PersonListResource

app = Flask(__name__)
app.config.from_file("../config.json", load=json.load)
api = Api(app, prefix='/api/v1')

api.add_resource(PersonResource, '/persons/<string:person_id>')
api.add_resource(PersonListResource, '/persons')
api.add_resource(NextBirthdayPersonsResource, '/persons:next_birthday')

app.config.update({
    'APISPEC_SPEC': APISpec(
        title='Persons API',
        version='v1',
        plugins=[MarshmallowPlugin()],
        openapi_version='2.0.0'
    ),
    'APISPEC_SWAGGER_URL': '/swagger/',
    'APISPEC_SWAGGER_UI_URL': '/api/v1/docs/'
})
docs = FlaskApiSpec(app)

docs.register(PersonResource)
docs.register(PersonListResource)
docs.register(NextBirthdayPersonsResource)
