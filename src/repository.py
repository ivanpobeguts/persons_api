import csv
from collections import defaultdict
from datetime import datetime
from pathlib import Path
from typing import Dict, Generator, List, Optional

from flask import current_app as app

from .exceptions import DataError
from .models import Person

ROOT_DIR_PATH = Path(__file__).resolve().parent.parent


class PersonRepository:
    def __init__(self, datafiles_folder: str = 'datafiles'):
        try:
            datafiles_folder = app.config['DATAFILES_FOLDER']
        except KeyError:
            pass
        self.persons_by_id: Dict = {}
        self.persons_by_country: Dict = defaultdict(list)
        data_source_path = ROOT_DIR_PATH.joinpath(datafiles_folder)
        self.persons_data_path = data_source_path.joinpath('persons.csv')
        self.cities_data_path = data_source_path.joinpath('cities.csv')
        self.countries_data_path = data_source_path.joinpath('countries.csv')
        self.birthdates_data_path = data_source_path.joinpath('birthdates.csv')
        self._get_persons_data()

    def _get_persons_data(self):
        """Collects persons data from 4 files and wraps it into Person classes"""

        persons_by_city = defaultdict(list)

        for row in self._fetch_file_rows(self.persons_data_path):
            person_id = row['id']
            self.persons_by_id[person_id] = Person(id=person_id, name=row['name'])

        for row in self._fetch_file_rows(self.cities_data_path):
            city = row['city']
            person_id = row['id']
            try:
                person = self.persons_by_id[person_id]
                person.city = city
                persons_by_city[city].append(person.id)
            except KeyError:
                raise DataError(f"Data error: no person with id {person_id}")

        for row in self._fetch_file_rows(self.countries_data_path):
            city = row['city']
            country = row['country']
            try:
                person_ids = persons_by_city[city]
                for person_id in person_ids:
                    person = self.persons_by_id.get(person_id)
                    person.country = country
                    self.persons_by_country[country].append(person_id)
            except KeyError:
                raise DataError(f"Data error: no city with name {city}")

        for row in self._fetch_file_rows(self.birthdates_data_path):
            person_id = row['id']
            try:
                person = self.persons_by_id[person_id]
                person.birth_date = datetime.strptime(row['birthdate'], Person.BIRTH_DATE_FORMAT).date()
            except KeyError:
                raise DataError(f"Data error: no person with id {person_id}")

    @staticmethod
    def _fetch_file_rows(file_path: Path) -> Generator[Dict, None, None]:
        with open(file_path, newline='') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                yield row

    def get_by_id(self, person_id: str) -> Optional[Person]:
        return self.persons_by_id.get(person_id)

    def get(self) -> List[Person]:
        return list(self.persons_by_id.values())

    def get_by_country(self, country: str) -> Generator[Person, None, None]:
        for person_id in self.persons_by_country.get(country, []):
            yield self.persons_by_id[person_id]
