import pytest
from src.app import app
from src.exceptions import DataError
from src.repository import PersonRepository


def test_get_persons_data_ok(app_context, repo_result_persons, correct_data_path):
    app.config['DATAFILES_FOLDER'] = correct_data_path
    person_repo = PersonRepository()

    assert len(person_repo.persons_by_id) == 3
    assert list(person_repo.persons_by_id.values()) == repo_result_persons
    assert len(person_repo.persons_by_country) == 2
    assert person_repo.persons_by_country['Norfolk Island'] == [repo_result_persons[0].id]
    assert person_repo.persons_by_country['Kenya'] == [p.id for p in repo_result_persons[1:]]


def test_test_get_persons_data_error(app_context, incorrect_data_path):
    with pytest.raises(DataError) as e:
        app.config['DATAFILES_FOLDER'] = incorrect_data_path
        PersonRepository()
    assert e.value.args[0] == 'Data error: no person with id 1'


def test_get_by_id_ok(app_context, repo_result_persons, correct_data_path):
    app.config['DATAFILES_FOLDER'] = correct_data_path
    person_repo = PersonRepository()

    assert person_repo.get_by_id('921868957') == repo_result_persons[1]


def test_get_by_id_not_found(app_context, correct_data_path):
    app.config['DATAFILES_FOLDER'] = correct_data_path
    person_repo = PersonRepository()

    assert person_repo.get_by_id('1') is None


def test_get_ok(app_context, repo_result_persons, correct_data_path):
    app.config['DATAFILES_FOLDER'] = correct_data_path
    person_repo = PersonRepository()

    assert person_repo.get() == repo_result_persons


def test_get_not_found(app_context, empty_data_path):
    app.config['DATAFILES_FOLDER'] = empty_data_path
    person_repo = PersonRepository()

    assert person_repo.get() == []


def test_get_by_country_ok(app_context, repo_result_persons, correct_data_path):
    app.config['DATAFILES_FOLDER'] = correct_data_path
    person_repo = PersonRepository()

    assert list(person_repo.get_by_country('Kenya')) == repo_result_persons[1:]


def test_get_by_country_not_found(app_context, correct_data_path):
    app.config['DATAFILES_FOLDER'] = correct_data_path
    person_repo = PersonRepository()

    assert list(person_repo.get_by_country('Sweden')) == []
