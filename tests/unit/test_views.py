import pytest

from src.app import app
from src.exceptions import DataError


def test_get_person_by_id_ok(mocker, person):
    mocker.patch('src.repository.PersonRepository._get_persons_data')
    mocker.patch('src.repository.PersonRepository.get_by_id', return_value=person)
    client = app.test_client()
    response = client.get('api/v1/persons/123456', headers={"Content-Type": "application/json"})

    assert response.status_code == 200
    assert response.json == {
        'age': 28,
        'birth_date': '1993-09-07',
        'city': 'Stockholm',
        'country': 'Sweden',
        'id': '123456',
        'name': 'Ivan'
    }


def test_get_person_by_id_not_found(mocker):
    mocker.patch('src.repository.PersonRepository._get_persons_data')
    mocker.patch('src.repository.PersonRepository.get_by_id', return_value=None)
    client = app.test_client()
    response = client.get('api/v1/persons/1', headers={"Content-Type": "application/json"})

    assert response.status_code == 404


def test_get_person_by_id_data_error(mocker):
    mocker.patch('src.repository.PersonRepository._get_persons_data', side_effect=DataError)
    client = app.test_client()
    response = client.get('api/v1/persons/123456', headers={"Content-Type": "application/json"})

    assert response.status_code == 400


def test_get_person_by_id_unknown_data_error(mocker):
    mocker.patch('src.repository.PersonRepository._get_persons_data', side_effect=KeyError)
    client = app.test_client()
    response = client.get('api/v1/persons/123456', headers={"Content-Type": "application/json"})

    assert response.status_code == 400


def test_get_persons_by_country_ok(mocker, repo_result_persons):
    mocker.patch('src.repository.PersonRepository._get_persons_data')
    mocker.patch('src.repository.PersonRepository.get_by_country', return_value=repo_result_persons[1:])
    client = app.test_client()
    response = client.get('api/v1/persons?country=Kenya', headers={"Content-Type": "application/json"})

    assert response.status_code == 200
    assert len(response.json['data']) == 2
    assert response.json['previous'] == ''
    assert response.json['next'] == ''
    assert response.json['count'] == 2
    assert response.json['limit'] == 25


@pytest.mark.parametrize(
    ('url'),
    [
        ('api/v1/persons?order_by=age'),
        ('api/v1/persons?order_by=age&reverse=False'),
        ('api/v1/persons?order_by=age&reverse=false'),
        ('api/v1/persons?order_by=age&reverse=fAlSe')
    ]
)
def test_get_persons_age_order(mocker, repo_result_persons, url):
    mocker.patch('src.repository.PersonRepository._get_persons_data')
    mocker.patch('src.repository.PersonRepository.get', return_value=repo_result_persons)
    client = app.test_client()
    response = client.get(url, headers={"Content-Type": "application/json"})

    assert response.status_code == 200
    assert len(response.json['data']) == 3
    assert response.json['previous'] == ''
    assert response.json['next'] == ''
    assert response.json['count'] == 3
    assert response.json['limit'] == 25
    assert response.json['data'][0]['id'] == repo_result_persons[1].id
    assert response.json['data'][2]['id'] == repo_result_persons[0].id


@pytest.mark.parametrize(
    ('url'),
    [
        ('api/v1/persons?order_by=age&reverse=True'),
        ('api/v1/persons?order_by=age&reverse=1'),
        ('api/v1/persons?order_by=age&reverse=true')
    ]
)
def test_get_persons_reverse_age_order(mocker, repo_result_persons, url):
    mocker.patch('src.repository.PersonRepository._get_persons_data')
    mocker.patch('src.repository.PersonRepository.get', return_value=repo_result_persons)
    client = app.test_client()
    response = client.get(url, headers={"Content-Type": "application/json"})

    assert response.status_code == 200
    assert len(response.json['data']) == 3
    assert response.json['previous'] == ''
    assert response.json['next'] == ''
    assert response.json['count'] == 3
    assert response.json['limit'] == 25
    assert response.json['data'][0]['id'] == repo_result_persons[0].id
    assert response.json['data'][2]['id'] == repo_result_persons[1].id


@pytest.mark.parametrize(
    ('url'),
    [
        ('api/v1/persons'),
        ('api/v1/persons?start=1&limit=2'),
    ]
)
def test_get_persons_pagination(app_context, mocker, repo_result_persons, url):
    app.config['PAGE_SIZE'] = 2
    mocker.patch('src.repository.PersonRepository._get_persons_data')
    mocker.patch('src.repository.PersonRepository.get', return_value=repo_result_persons)
    client = app.test_client()
    response = client.get(url, headers={"Content-Type": "application/json"})

    assert response.status_code == 200
    assert len(response.json['data']) == 2
    assert response.json['previous'] == ''
    assert response.json['next'].endswith('api/v1/persons?start=3&limit=2')
    assert response.json['count'] == 3
    assert response.json['limit'] == 2


def test_get_persons_data_error(mocker):
    mocker.patch('src.repository.PersonRepository._get_persons_data', side_effect=DataError)
    client = app.test_client()
    response = client.get('api/v1/persons', headers={"Content-Type": "application/json"})

    assert response.status_code == 400


def test_get_persons_unknown_data_error(mocker):
    mocker.patch('src.repository.PersonRepository._get_persons_data', side_effect=KeyError)
    client = app.test_client()
    response = client.get('api/v1/persons', headers={"Content-Type": "application/json"})

    assert response.status_code == 400


def test_get_next_birtday_persons_ok(mocker, repo_result_persons):
    mocker.patch('src.repository.PersonRepository._get_persons_data')
    mocker.patch('src.repository.PersonRepository.get', return_value=repo_result_persons)
    client = app.test_client()
    response = client.get('api/v1/persons:next_birthday', headers={"Content-Type": "application/json"})

    assert response.status_code == 200
    assert len(response.json) == 1
    assert response.json == [{
        'age': 66,
        'birth_date': '1955-06-06',
        'city': 'East Michael',
        'country': 'Norfolk Island',
        'id': '972830059',
        'name': 'Spencer Eaton'
    }]


def test_get_next_birtday_persons_data_error(mocker):
    mocker.patch('src.repository.PersonRepository._get_persons_data', side_effect=DataError)
    client = app.test_client()
    response = client.get('api/v1/persons:next_birthday', headers={"Content-Type": "application/json"})

    assert response.status_code == 400


def test_get_next_birtday_persons_unknown_data_error(mocker):
    mocker.patch('src.repository.PersonRepository._get_persons_data', side_effect=KeyError)
    client = app.test_client()
    response = client.get('api/v1/persons:next_birthday', headers={"Content-Type": "application/json"})

    assert response.status_code == 400
