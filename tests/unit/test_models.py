import pytest


@pytest.mark.freeze_time
@pytest.mark.parametrize(
    ('now_date', 'age'),
    [
        ('2021-10-09', 28),
        ('1993-10-07', 0),
        ('2023-09-10', 30)
    ]
)
def test_calculate_age_ok(person, freezer, now_date, age):
    freezer.move_to(now_date)
    assert person.age == age


@pytest.mark.freeze_time
@pytest.mark.parametrize(
    ('now_date', 'result_days'),
    [
        ('2021-10-09', 333),
        ('2021-09-06', 1),
        ('2021-09-08', 364)
    ]
)
def test_time_before_birthday_ok(person, freezer, now_date, result_days):
    freezer.move_to(now_date)
    assert person._days_before_birthday == result_days
