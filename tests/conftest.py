from datetime import date
from pathlib import Path

from pytest import fixture

from src.app import app
from src.models import Person


@fixture
def app_context():
    with app.app_context():
        yield


@fixture
def person():
    return Person(
        id='123456',
        name='Ivan',
        birth_date=date(1993, 9, 7),
        city='Stockholm',
        country='Sweden'
    )


@fixture
def repo_result_persons():
    return [
        Person(
            id='972830059',
            name='Spencer Eaton',
            birth_date=date(1955, 6, 6),
            city='East Michael',
            country='Norfolk Island'
        ),
        Person(
            id='921868957',
            name='Hannah Gallegos',
            birth_date=date(2004, 7, 31),
            city='Carsonhaven',
            country='Kenya'
        ),
        Person(
            id='926822598',
            name='Amanda French',
            birth_date=date(1995, 6, 20),
            city='Theresaville',
            country='Kenya'
        ),
    ]


@fixture
def correct_data_path():
    return 'tests/integration/files/correct'


@fixture
def incorrect_data_path():
    return 'tests/integration/files/incorrect'


@fixture
def empty_data_path():
    return Path(__file__).resolve().parent.joinpath('integration/files/empty')
